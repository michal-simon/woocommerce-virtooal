<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require_once( 'interface-virtooal-try-on-mirror.php' );
require_once( 'class-virtooal-try-on-mirror.php' );
require_once( 'class-virtooal-try-on-mirror-api.php' );
/*
* Virtooal_Try_On_Mirror_Admin class for setting virtooal settings.
*/
class Virtooal_Try_On_Mirror_Admin extends Virtooal_Try_On_Mirror implements Virtooal_Try_On_Mirror_Interface
{
	//Set up base actions
	public function init() {
		add_action( 'admin_menu', array( $this, 'admin_menu' ) );

		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );

		add_action( 'admin_post_virtooal_api_login_response', array( $this, 'api_login_response' ));
		add_action( 'admin_post_virtooal_api_logout_response', array( $this, 'api_logout_response' ));
		add_action( 'admin_post_virtooal_settings_response', array( $this, 'settings_response' ));
		add_action( 'admin_post_virtooal_product_feed_response', array( $this, 'product_feed_response' ));
	}

	//Add plugin to woocommerce admin menu.
	public function admin_menu() {
		add_submenu_page( 'woocommerce', 'Virtooal', 'Virtooal', 'manage_options', 'virtooal', array( $this, 'admin_pages' ) );
	}

	#region META BOX
	/**
     * Adds the meta box container.
     */
    public function add_meta_box( $post_type ) {

		if ( !$this->api ) {
			return;
		}
        // Limit meta box to certain post types.
        $post_types = array( 'product' );
 
        if ( in_array( $post_type, $post_types ) ) {
            add_meta_box(
                'virtooal_product_meta_box_name',
                __( 'Virtooal', 'textdomain' ),
                array( $this, 'render_meta_box_content' ),
                $post_type,
                'side',
                'high'
            );
        }
	}
	
	/**
     * Render Meta Box content.
     *
     * @param WP_Post $post The post object.
     */
    public function render_meta_box_content( $post ) {

		if ( !$this->api ) {
			return;
		}
        global $woocommerce;
		$product_id = get_the_ID();
		$product = wc_get_product( $product_id );
		$query_data = array(
			'url' => get_permalink( $product_id ),
			'title' => $product->get_name(),
			'product_id' => $product_id,
			'iframe' => 1,
			'platform' => 6,
		);
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );
		if ($image) {
			$query_data['img'] = $image[0];
		}
		$virtooal_api = new Virtooal_Try_On_Mirror_Api();
		$response = $virtooal_api->get_product( $product_id );
		$in_virtooal_db = false; 
		$published = null;
		if( $response['http_code'] == 200 ){ 
			$in_virtooal_db = true;
			$published = $response['body']['product']['published'];
		}
		$this->render( 'admin/product-meta-box.php', array(
			'in_virtooal_db' => $in_virtooal_db,
			'published' => $published,
			'query_data' => http_build_query( $query_data ),
		) );
	}
	#endregion META BOX
	
	//Output form with settings.
	public function admin_pages() {
		$tab = ( ! empty( $_GET['tab'] ) ) ? esc_attr( $_GET['tab'] ) : 'settings';
		$title =  __( 'Virtooal Settings', 'virtooal-try-on-mirror' );
		$site_url = home_url();
		$admin_url = admin_url();
		$this->render( 'admin/settings-content.php', array(
			'api_logged_in' 	  => (bool)$this->api['access_token'],
			'tab'             	  => $tab,
			'title'           	  => $title,
			'site_url'        	  => $site_url,
			'admin_url'       	  => $admin_url,
			'virtooal_categories' => $this->get_virtooal_categories(),
			'virtooal_genders'    => $this->get_virtooal_genders(),
			'store_categories'    => $this->get_store_categories(),
			'data' 				  => get_option( 'virtooal_'.$tab )
		) );
	}
	#region AUTH
	public function api_login_response() {
		$this->verify_nonce( 'api_login' );
		$error_message = null;
		$public_api_key = sanitize_text_field( $_POST['virtooal']['public_api_key'] );
		$private_api_key = sanitize_text_field( $_POST['virtooal']['private_api_key'] );
		$virtooal_api = new Virtooal_Try_On_Mirror_Api();
		$response = $virtooal_api->login($public_api_key,$private_api_key);
		
		if( $response['http_code'] == 200 ){
			
			update_option( 'virtooal_api', array(
				'public_api_key' => $public_api_key,
				'access_token' => $response['body']['access_token'],
				'refresh_token' => $response['body']['refresh_token'],
				'user_id' => $response['body']['user_id'],
			) );
			$response = $virtooal_api->get_settings();
			if(!$response['success']) {
				$error_message = $response['message'];
			}
		} else {
			if( isset( $response['body']['message'] ) ) {
				$error_message = $response['body']['message'];
			} 
			else if( isset( $response['body']['private_api_key'] ) ) {
				$error_message = $response['body']['private_api_key'];
			}
		}
		$this->custom_redirect( 'api', $error_message );
	}

	public function api_logout_response() {
		$this->verify_nonce( 'api_logout' );
		$error_message = null;
		$virtooal_api = new Virtooal_Try_On_Mirror_Api();
		$response = $virtooal_api->logout();
		
		delete_option( 'virtooal_api');
		if( $response['http_code'] == 200 ) {
			delete_option( 'virtooal_api');
		} else {
			if( isset( $response['body']['message'] ) ) {
				$error_message = $response['body']['message'];
			} 
		}
		$this->custom_redirect( 'api', $error_message );
	}
	#endregion AUTH

	#region SETTINGS
	public function settings_response() {
		$this->verify_nonce( 'settings' );
		$virtooal_settings = get_option( 'virtooal_settings' );
		$virtooal_settings['tryon_text'] = sanitize_text_field( $_POST['virtooal_settings']['tryon_text'] );
		$virtooal_settings['only_wc_pages'] = isset( $_POST['virtooal_settings']['only_wc_pages'] ) ? 1 : 0;
		$virtooal_settings['add_open_div'] = isset( $_POST['virtooal_settings']['add_open_div'] ) ? 1 : 0;
		$virtooal_settings['tryon_show_catalog_page'] = isset( $_POST['virtooal_settings']['tryon_show_catalog_page'] ) ? 1 : 0;
		$virtooal_settings['tryon_show_product_page'] = isset( $_POST['virtooal_settings']['tryon_show_product_page'] ) ? 1 : 0;
		
		update_option( 'virtooal_settings', $virtooal_settings );
		$virtooal_api = new Virtooal_Try_On_Mirror_Api();

		$data = array(
			'tryon_text' => $_POST['virtooal_settings']['tryon_text']
		);
		$response = $virtooal_api->post_settings( $data );
		$error_message = null;
		if( isset( $response['body']['message'] ) && $response['http_code'] != 200 ) {
			$error_message = $response['body']['message'];
		} 
		$this->custom_redirect( 'settings', $error_message);
	}
	#endregion SETTINGS

	#region SETTINGS
	public function product_feed_response() {
		$this->verify_nonce( 'product_feed' );
		update_option( 'virtooal_product_feed', $_POST['virtooal_product_feed'] );
		$this->custom_redirect( 'product_feed' );
	}
	#endregion SETTINGS

	private function custom_redirect( $tab, $message = null ) {
		wp_redirect( esc_url_raw( add_query_arg( array(
			'response' => $message ? $message : 'success',
			),
			admin_url('admin.php?page=virtooal&tab='.$tab ) 
		) ) );
	}

	private function verify_nonce( $name ) {
		if( !isset( $_POST['virtooal_' . $name . '_nonce'] ) ||
			!wp_verify_nonce( 
				$_POST['virtooal_' . $name . '_nonce'], 
				'virtooal_' . $name . '_form_nonce'
			) ) {
			die('nonce');		
		}
	}

	private function get_store_categories() {
		$cat_args = array(
			'orderby'    => 'name',
			'order'      => 'asc',
			'hide_empty' => false,
		);

		$product_categories = get_terms( 'product_cat', $cat_args );

		$sorted_categories = [];
		foreach ( $product_categories as $product_category ) {
			$ancestors = get_term_parents_list(
				$product_category->term_id, 
				'product_cat', 
				array(
					'separator' => ' > ', 
					'link' => false, 
					'inclusive' => false
				)
			);
			$sorted_categories[$ancestors.$product_category->name] = $product_category;
		}
		ksort( $sorted_categories );

		return $sorted_categories;
	}

	private function get_virtooal_genders() {
		return array(
			'W' => 'Women',
			'M' => 'Men',
			'U' => 'Unisex',
			'B' => 'Boys',
			'G' => 'Girls',
			'K' => 'Kids Unisex',
		);
	}

	private function get_virtooal_categories() {
		return array(
			'eyewear' => array(
				10 => 'Contact lenses',
				11 => 'Glasses',
				16 => 'Sunglasses',
			),
			'cosmetics' => array(
				6 => 'Blush',
				12 => 'Concealer',
				7 => 'Eye liner',
				8 => 'Eye shadow',
				14 => 'Facelift',
				4 => 'Foundation',
				9 => 'Hair color',
				1 => 'Lashes',
				5 => 'Lip Gloss',
				3 => 'Lipliner',
				2 => 'Lipstick',
				13 => 'Self tanning solution',
				15 => 'Teethwhitening',
			),
			'jewellery' => array(
				19 => 'Earrings',
				20 => 'Necklaces',
				21 => 'Piercing',
			),	
			'accessories' => array(
				17 => 'Hats',
				18 => 'Scarves',
			),		
			/*22 => 'Tops/Shirts',
			23 => 'Jackets/Coats',
			24 => 'Dresses',
			25 => 'Skirts/Shorts',
			26 => 'Pants/Jeans',
			27 => 'Tops/Shirts',
			28 => 'Jackets/Coats',
			29 => 'Pants/Jeans',
			30 => 'Shorts',*/
		);
	}
}
