<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Virtooal_Try_On_Mirror_Feed class for generating Virtooal product XML feed.
*/
class Virtooal_Try_On_Mirror_Feed
{
    public function generate_feed() {

        $options = get_option('virtooal_product_feed');
        if(!(int)$options['status']) {
            return;
        }
        header('Content-Type: application/xml; charset=utf-8', true);
        $args = array( 
            'post_type' => array( 'product', 'product_variation' ),
            'posts_per_page' => -1,
            'post_status' => $options['post_statuses'],
        );
        $loop = new WP_Query( $args );
        echo '<?xml version="1.0" encoding="UTF-8"?>
        <SHOP>';
        while ( $loop->have_posts() ) : 
            $loop->the_post();
            global $product;

            $terms = get_the_terms( $loop->post->ID , 'product_cat' );
            if(get_post_type($loop->post->ID) == 'product_variation') {
                $terms = get_the_terms( $loop->post->post_parent , 'product_cat' );
            }
            $cat = null;
            $gender = null;
            $export = 1;
            foreach($terms as $term) {
                $term_id = $term->term_id;
                if(isset($options['categories'][$term_id]) 
                && $options['categories'][$term_id] && !$cat) {
                    $cat = $options['categories'][$term_id];
                }

                if(isset($options['genders'][$term_id]) 
                && $options['genders'][$term_id] && !$gender) {
                    $gender = $options['genders'][$term_id];
                }
                if(isset($options['export'][$term_id])
                && $options['export'][$term_id] != '') {
                    $export &= $options['export'][$term_id];
                }
            }
            if(!$export) {
                continue;
            }

            $cat = $cat ?: $options['default_category'];
            $gender = $gender ?: $options['default_gender'];
            $group_id = $loop->post->post_parent ?: '';

            echo '<SHOPITEM>
                <ITEM_ID>'.$loop->post->ID.'</ITEM_ID>
                <GROUP_ID>' . $group_id . '</GROUP_ID>
                <PRODUCTNAME><![CDATA[' . get_the_title() . ']]></PRODUCTNAME>
                <DESCRIPTION><![CDATA[' . strip_shortcodes(get_the_excerpt()) . ']]></DESCRIPTION>
                <URL><![CDATA[' . get_the_permalink() . ']]></URL>
                <CATEGORY>' . $cat . '</CATEGORY>
                <IMGURL><![CDATA[' . wp_get_attachment_url( $product->get_image_id() ) . ']]></IMGURL>
                <PRICE>' . $product->get_price() . '</PRICE>
                <SEX>' . $gender . '</SEX>
                <EAN><![CDATA[' . $product->get_sku() . ']]></EAN>
            </SHOPITEM>';
        endwhile;
        echo '</SHOP>';
        
    }
}