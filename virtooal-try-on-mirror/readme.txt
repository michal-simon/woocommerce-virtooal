=== Virtooal Try-on Mirror [Premium Version]===
Contributors: virtooal
Donate link: https://try.virtooal.com
Tags: virtual mirror, lipsticks, eye shadows, blushes, eyeliners, glasses, sunglasses, contact lenses, woocommerce
Requires at least: 4.7
Tested up to: 5.7.2
WC requires at least: 3.0.0
WC tested up to: 5.4.1
Requires PHP: 5.4
Stable tag: trunk
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

The Virtual mirror allows the shoppers to experience all decorative cosmetics, sunglasses, contact lenses, jewelry, 
clothing and apparel using their own photos or photos of models. It is suitable for all shops. 
The editing process of the profile picture works as a process of auto-detection of the face and its features (lips, eyes, etc.), 
no real time editing needed.

== Description ==

**[Virtooal](https://try.virtooal.com/en/)** team with the base in London has been developing AR technology since 2008. It focuses on bringing the virtual TRY-ON into web browsers as its plugins are integrated directly into fashion and beauty e-commerce websites. 

The last advantage street stores had over online stores is now a thing of the past! With Virtooal’s Magic Mirror, customers can try on products directly from the comfort of their homes, using their webcams. Give your customers realistic shopping experiences with Virtual Try-On! Augmented reality designed for eyewear, fashion, and cosmetics stores brings fun to online shopping. 

The Magic Mirror is a plugin that can be integrated into e-commerce websites by embedding a short HTML code. The mirror has several parts. A floating icon in the bottom corner of the page and a “Try On” button that triggers the mirror and puts the product on the customers’ photos or directly on their face in the live camera version.


== Other Notes ==

= Account & Pricing =

Our paid monthly subscription plans start from £89/€99/$109. You can also opt to pay £179/€199/$219 for the initial setup (including the first 100 products or 1000 decorative cosmetics) or set everything up yourself and then pay only the monthly subscription. The Virtooal account is not created during extension installation. To create an account please visit the [signup page](https://try.virtooal.com/en/checkout/register). For more details please visit the company [payment page](https://try.virtooal.com/en/pricing) or [email us](https://try.virtooal.com/en/contact) for individual pricing options.

= Features =

* supports prescription and sunglasses, colored and themed contact lenses, decorative cosmetics, necklaces, earrings, hats, scarves and many more
* HTML5 technology - works flawlessly with Android, iS, Apple and desktop browsers
* Full LIVE visualization of the products with automatic face detection for easier alignment
* simple products integration from photos you already have in your store
* no programming skills needed
* we can do all the integration for you in no more than 48 hours
* customizable graphic layout and the size of the plugin
* avatars/models included and can be added/changed per your preference

= Benefits For Your Shop =

* a boost in conversion rates by up to 50%
* money-back guarantee if the conversion rate is not increased by at least 10%
* enhanced customer’s shopping experience
* significantly reduces the number of product returns and complaints
* more items in the basket
* better engagement to complete purchases
* direct social media sharing for more organic traffic
* a cutting-edge, fun to use e-shop that customers will want to return to and recommend

= Live Demo =

Test our solution in a fully working [demo store](https://try.virtooal.com/en/demo-store). 

== Frequently Asked Questions ==

**For additional information check https://try.virtooal.com/en/knowledge-base**


== Changelog ==

= 1.0.0 =
* Welcome our very first version of the plugin!

= 1.1.0 =
* Added retrieving of settings from virtooal system

= 1.1.3 =
* Added automirror option

= 1.1.4 =
* Fixed a bug where try on buttons were inserted when automirror was enabled

= 1.1.5 =
* replaced Virtooal Setup Administration iframe on the WooCommerce product edit page with a link to the administration
* made changes in the code to comply with the WordPress Plugin Guidelines

= 1.1.6 =
* Fixed try on button not showing

= 1.1.9 =
* Added Settings to plugin action links
* Fixed magic mirror widget showing when automirror turned on
* Tested for WP 5.5, WC 4.7.0

= 1.1.10 =
* Tested for WP 5.6.1, WC 5.0.0

= 1.2.0 =
* added class *virtooal-tryon-btn-wrapper* to "Try On" button wrapper div element for easier customization
* added "Try on button text" option in the settings
* added option to show the mirror only on WooCommerce related pages (product page, catalogue page, etc.)
* added Virtooal meta box to product edit page
* added new, more secure Virtooal API
* removed automirror enabling option
* other minor fixes and improvements
* premium version available

= 1.2.1 =
* Added status option (Enabled/Disabled) to xml product feed
* Fixed admin settings tab URLs to support Wordpress sites installed in subfolder

= 1.2.6 =
* Added gender selector
* Added product export filtering by category

= 1.2.7 =
* Added shortcode removing from xml feed description tag
* Fixed product variations not filtered by category in the xml feed

= 1.2.8 =
* Added settings for "Try On" button placement
* Added option to select post statuses in xml feed settings

= 1.2.9 =
* Added settings for "Try On" button to show/hide on product and catalog pages
* Minor fixes

== Upgrade Notice ==

= 1.2.0 =
After upgrading from version 1.1.* to version 1.2.*, you will need to fill your API credentials and connect to our new Virtooal API on the plugin's "Settings > Virtooal API Connection" tab.
You can find the public and private API keys in your Virtooal account on the "Billing information / Profile settings" page. 