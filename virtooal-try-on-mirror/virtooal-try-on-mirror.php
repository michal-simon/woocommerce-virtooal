<?php
/**
 * Plugin Name: Virtooal Try-on Mirror [Premium Version]
 * Plugin URI: http://wordpress.org/plugins/virtooal-try-on-mirror/
 * Description: This plugin allows to quickly install Virtooal Try-on Mirror on any WooCommerce website.
 * Version: 1.2.9
 * WC requires at least: 3.0.0
 * WC tested up to: 4.7.0
 * Author: Virtooal
 * Author URI: https://try.virtooal.com
 * Text Domain: virtooal-try-on-mirror
 * Copyright: © 2020 Virtooal.
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( !defined( 'VIRTOOAL_TRY_ON_MIRROR_VERSION' ) ) {
	define( 'VIRTOOAL_TRY_ON_MIRROR_VERSION', '1.2.9' );
}

function virtooal_try_on_mirror_activation() {

	update_option('virtooal_try_on_mirror_version', VIRTOOAL_TRY_ON_MIRROR_VERSION);

	$installation_id = get_option('virtooal_installation_id');
	if(!$installation_id) {
		$installation_id = wp_generate_uuid4();
		update_option('virtooal_installation_id',$installation_id);
	}

	$default_virtooal_settings = [
		'add_open_div' => 0,
		'automirror' => 0,
		'only_wc_pages' => 0,
		'tryon_text' => '',
		'tryon_show_catalog_page' => 1,
		'tryon_show_product_page' => 1,
	];
	$virtooal_settings = get_option( 'virtooal_settings', array() );
	update_option( 'virtooal_settings', array_merge( $default_virtooal_settings, $virtooal_settings ) );

	$default_virtooal_api = [
		'public_api_key' => '',
		'access_token' => '',
		'refresh_token' => '',
		'user_id' => ''
	];
	$virtooal_api = get_option( 'virtooal_api', array() );
	update_option( 'virtooal_api', array_merge( $default_virtooal_api, $virtooal_api ) );

	$default_virtooal_product_feed = [
		'status' => 0,
		'post_statuses' => array('publish','draft','pending','future'),
		'default_category' => 0,
		'default_gender' => 'U',
		'categories' => array(),
		'genders' => array(),
		'export' => array()

	];
	$virtooal_product_feed = get_option( 'virtooal_product_feed', array() );
	update_option( 'virtooal_product_feed', array_merge( $default_virtooal_product_feed, $virtooal_product_feed ) );
	global $wp_version;
	$virtooal_api = get_option( 'virtooal_api', array() );
	require_once( dirname( __FILE__ ) . '/src/class-virtooal-try-on-mirror-api.php' );
	$api = new Virtooal_Try_On_Mirror_Api();
	$api->post_user( array(
		'plugin_version' => VIRTOOAL_TRY_ON_MIRROR_VERSION,
		'domain' => get_site_url(),
		'wordpress_version' => $wp_version,
		'updated_at' => date('Y-m-d H:i:s'),
		'partner_id' => $virtooal_api['user_id'],
		'premium' => 1,
		'id' => $installation_id,
	) );
}

register_activation_hook(__FILE__, 'virtooal_try_on_mirror_activation');

function virtooal_check_version() {
	if ( VIRTOOAL_TRY_ON_MIRROR_VERSION !== get_option( 'virtooal_try_on_mirror_version' ) ) {
		virtooal_try_on_mirror_activation();
	}
    
}

add_action('plugins_loaded', 'virtooal_check_version');

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	// Check if there is admin user
	if ( is_admin() ) {
		require_once( dirname( __FILE__ ) . '/src/class-virtooal-try-on-mirror-admin.php' );
		$virtooal_try_on_mirror = new Virtooal_Try_On_Mirror_Admin();
	} else {
		require_once( dirname( __FILE__ ) . '/src/class-virtooal-try-on-mirror.php' );
		$virtooal_try_on_mirror = new Virtooal_Try_On_Mirror();
	}

	$virtooal_try_on_mirror->init();
} else {
	wp_die( 'Sorry, but this plugin requires the WooCommerce Plugin to be installed and active. 
			<br><a href="' . admin_url( 'plugins.php' ) . '">&laquo; Return to Plugins</a>' );
}

add_action( 'admin_init', 'virtooal_has_woocommerce' );
function virtooal_has_woocommerce() {
    if ( is_admin() && current_user_can( 'activate_plugins' ) && !is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
        add_action( 'admin_notices', 'virtooal_notice' );

        deactivate_plugins( plugin_basename( __FILE__ ) );

        if ( isset( $_GET['activate'] ) ) {
            unset( $_GET['activate'] );
        }
	}
}

add_action( 'init', 'virtooal_update_settings' );
function virtooal_update_settings() {
    if ( isset( $_GET['virtooal_settings_update'] ) ) {
		require_once( dirname( __FILE__ ) . '/src/class-virtooal-try-on-mirror-api.php' );
		$virtooal_api = new Virtooal_Try_On_Mirror_Api();
		$virtooal_api->get_settings();
	}
}

add_action( 'init', 'virtooal_xml_product_feed' );
function virtooal_xml_product_feed() {
    if ( isset( $_GET['virtooal_xml_product_feed'] ) ) {
		require_once( dirname( __FILE__ ) . '/src/class-virtooal-try-on-mirror-feed.php' );
		$feed = new Virtooal_Try_On_Mirror_Feed();
		$feed->generate_feed();
		die();
	}	
}

function virtooal_notice(){
	echo '<div class="error"><p>' . __( 
		'Sorry, but Virtooal Try-on Mirror requires WooCommerce version 3.0.0 or above to be installed and active.', 
		'virtooal-try-on-mirror' 
	) .'</p></div>';
}

add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), 'virtooal_add_action_links' );
function virtooal_add_action_links ( $links ) {
	$mylinks = array(
	'<a href="' . admin_url( 'admin.php?page=virtooal' ) . '">Settings</a>',
	);

	return array_merge( $mylinks, $links );
}

// add plugin upgrade notification
add_action( 'in_plugin_update_message-virtooal-try-on-mirror/virtooal-try-on-mirror.php', 'virtooal_show_upgrade_notification', 10, 2 );
function virtooal_show_upgrade_notification( $currentPluginMetadata, $newPluginMetadata ) {
   // check "upgrade_notice"
  if ( isset( $newPluginMetadata->upgrade_notice ) && strlen( trim( $newPluginMetadata->upgrade_notice ) ) > 0 ){
	echo '<div style="background-color: rgba(255, 185, 0, .5); padding: 10px; margin-top: 10px"><strong>Important Upgrade Notice:</strong>';
    echo esc_html( $newPluginMetadata->upgrade_notice ), '</div>';
  }
}

add_filter('plugins_api', 'virtooal_plugin_info', 20, 3);
/*
 * $res empty at this step
 * $action 'plugin_information'
 * $args stdClass Object ( [slug] => woocommerce [is_ssl] => [fields] => Array ( [banners] => 1 [reviews] => 1 [downloaded] => [active_installs] => 1 ) [per_page] => 24 [locale] => en_US )
 */
function virtooal_plugin_info( $res, $action, $args ){
 
	$plugin_slug = 'virtooal-try-on-mirror'; // we are going to use it in many places in this function
 
	// do nothing if this is not about getting plugin information or if it is not our plugin
	if( 'plugin_information' !== $action || $plugin_slug !== $args->slug ) {
		return false;
	}

	// trying to get from cache first
	if( false == $remote = get_transient( 'virtooal_update_' . $plugin_slug ) ) {
 
		// info.json is the file with the actual plugin information on your server
		$remote = wp_remote_get( 'https://try.virtooal.com/uploads/plugins/woocommerce/info.json', array(
			'timeout' => 10,
			'headers' => array(
				'Accept' => 'application/json'
			) )
		);
 
		if ( ! is_wp_error( $remote ) && isset( $remote['response']['code'] ) && $remote['response']['code'] == 200 && ! empty( $remote['body'] ) ) {
			set_transient( 'virtooal_update_' . $plugin_slug, $remote, 120 ); // 2 minutes
		}
 
	}
 
	if( ! is_wp_error( $remote ) && isset( $remote['response']['code'] ) && $remote['response']['code'] == 200 && ! empty( $remote['body'] ) ) {
 
		$remote = json_decode( $remote['body'] );
		$result = new stdClass();
 
		$result->name = $remote->name;
		$result->slug = $plugin_slug;
		$result->version = $remote->version;
		$result->tested = $remote->tested;
		$result->requires = $remote->requires;
		$result->author = '<a href="https://virtooal.com">Virtooal</a>';
		$result->author_profile = 'https://profiles.wordpress.org/virtooal';
		$result->download_link = $remote->download_url;
		$result->trunk = $remote->download_url;
		$result->requires_php = '5.4';
		$result->last_updated = $remote->last_updated;
		$result->sections = array(
			'description' => $remote->sections->description,
			'installation' => $remote->sections->installation,
			'changelog' => $remote->sections->changelog
			// you can add your custom sections (tabs) here
		);
 
		// in case you want the screenshots tab, use the following HTML format for its content:
		// <ol><li><a href="IMG_URL" target="_blank"><img src="IMG_URL" alt="CAPTION" /></a><p>CAPTION</p></li></ol>
		if( !empty( $remote->sections->screenshots ) ) {
			$result->sections['screenshots'] = $remote->sections->screenshots;
		}
 
		$result->banners = array(
			'low' => 'https://try.virtooal.com/uploads/plugins/woocommerce/banner-772x250.png',
			'high' => 'https://try.virtooal.com/uploads/plugins/woocommerce/banner-1544x500.png'
		);
		return $result;
 
	}
 
	return false;
 
}

add_filter('site_transient_update_plugins', 'virtooal_push_update' );
 
function virtooal_push_update( $transient ){
 
	if ( empty($transient->checked ) ) {
            return $transient;
        }
 
	// trying to get from cache first, to disable cache comment 10,20,21,22,24
	$remote = get_transient( 'virtooal_upgrade_virtooal-try-on-mirror' );
 
	if( $remote ) {
 
		$remote = json_decode( $remote['body'] );
		
		// your installed plugin version should be on the line below! You can obtain it dynamically of course 
		if( $remote && version_compare(VIRTOOAL_TRY_ON_MIRROR_VERSION, $remote->version, '<' ) ) {
			$res = new stdClass();
			$res->slug = 'virtooal-try-on-mirror';
			$res->plugin = 'virtooal-try-on-mirror/virtooal-try-on-mirror.php'; // it could be just YOUR_PLUGIN_SLUG.php if your plugin doesn't have its own directory
			$res->new_version = $remote->version;
			$res->tested = $remote->tested;
			$res->package = $remote->download_url;
           	$transient->response[$res->plugin] = $res;
        }
 
	} else {
		// info.json is the file with the actual plugin information on your server
		$remote = wp_remote_get( 'https://try.virtooal.com/uploads/plugins/woocommerce/info.json', array(
			'timeout' => 10,
			'headers' => array(
				'Accept' => 'application/json'
			) )
		);
 
		if ( !is_wp_error( $remote ) && isset( $remote['response']['code'] ) && $remote['response']['code'] == 200 && !empty( $remote['body'] ) ) {
			set_transient( 'virtooal_upgrade_virtooal-try-on-mirror', $remote, 1800 ); // 12 hours cache
		}
	}

    return $transient;
}

add_action( 'upgrader_process_complete', 'virtooal_after_update', 10, 2 );
 
function virtooal_after_update( $upgrader_object, $options ) {
	if ( $options['action'] == 'update' && $options['type'] === 'plugin' )  {
		// just clean the cache when new plugin version is installed
		delete_transient( 'virtooal_upgrade_virtooal-try-on-mirror' );
	}
}