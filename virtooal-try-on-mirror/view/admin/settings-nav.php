<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$active = 'nav-tab-active';
?>

<nav class="nav-tab-wrapper wp-clearfix" aria-label="Secondary menu">
    <a href="<?php echo site_url() ?>/wp-admin/admin.php?page=virtooal&tab=settings" class="nav-tab <?= $tab == 'settings' ? $active : '' ?>">Settings</a>
    <a href="<?php echo site_url() ?>/wp-admin/admin.php?page=virtooal&tab=product_feed" class="nav-tab <?= $tab == 'product_feed' ? $active : '' ?>">XML Product Feed</a>
    <a href="<?php echo site_url() ?>/wp-admin/admin.php?page=virtooal&tab=api" class="nav-tab <?= $tab == 'api' ? $active : '' ?>">Virtooal API Connection</a>
</nav>

