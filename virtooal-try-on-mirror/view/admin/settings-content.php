<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="wrap">

<h1 clas="wp-heading-inline"><?= $title ?></h1>

<?php
 if ( !$api_logged_in ) {
    echo '
    <div class="notice is-dismissible notice-warning">
        <p>' . 
        __( 'Signup for a free Virtooal account at' , 'virtooal-try-on-mirror' ).' <a href="https://www.virtooal.com/en/pricing-platforms" target="_blank">www.virtooal.com</a>, ' .
        __( 'then copy and paste Public and Private API keys from', 'virtooal-try-on-mirror' ).' <a href="http://setup.virtooal.com/en/auth/profile" target="_blank">' .
        __( 'profile\'s page', 'virtooal-try-on-mirror' ).'</a> ' .
        __( 'into the API Connection form.', 'virtooal-try-on-mirror' ).'
        </p>
    </div>';
}
if( isset( $_GET['response'] ) ) {
    if( esc_attr( $_GET['response'] ) === 'success' ) {
        echo '<div class="notice is-dismissible notice-success">
        <p>'
            . __( 'Saved successfully!', 'virtooal-try-on-mirror' ) .
        '</p>
        </div>';
    } else {
        echo '<div class="notice is-dismissible notice-error">
        <p>'
            . __( $_GET['response'], 'virtooal-try-on-mirror' ) .
        '</p>
        </div>';
    }
}
include dirname( __FILE__ ) . '/settings-nav.php';
$formAction = esc_url( admin_url( 'admin-post.php' ) );

if( $tab == 'settings' ): ?>
    <h2>General Settings</h2>
    <form action="<?php echo $formAction; ?>" method="post" id="virtooal_settings_form" >
        <input type="hidden" name="action" value="virtooal_settings_response">
        <input type="hidden" name="virtooal_settings_nonce" value="<?php echo wp_create_nonce( 'virtooal_settings_form_nonce' ) ?>"/>
        <table class="form-table" aria-label="General Settings">
            <tbody>
                <tr>
                    <th scope="row">
                       Virtual Mirror
                    </th>
                    <td>
                        <fieldset>
                            <label for="virtooal-only_wc_pages">
                                <input type="checkbox" <?php checked( $data['only_wc_pages'] == 1 ) ?> name="virtooal_settings[only_wc_pages]" id="virtooal-only_wc_pages" value="1">
                                Show Virtual Mirror only on WooCommerce pages
                            </label>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                       "Try On" button position
                    </th>
                    <td>
                        <fieldset>
                            <label for="virtooal-tryon_show_catalog_page">
                                <input type="checkbox" <?php checked( $data['tryon_show_catalog_page'] == 1 ) ?> name="virtooal_settings[tryon_show_catalog_page]" id="virtooal-tryon_show_catalog_page" value="1">
                                Show "Try On" buttons on catalog page
                            </label>
                        </fieldset>
                        <fieldset>
                            <label for="virtooal-tryon_show_product_page">
                                <input type="checkbox" <?php checked( $data['tryon_show_product_page'] == 1 ) ?> name="virtooal_settings[tryon_show_product_page]" id="virtooal-tryon_show_product_page" value="1">
                                Show "Try On" button on product page
                            </label>
                        </fieldset>
                        <fieldset>
                            <label for="virtooal-add_open_div">
                                <input type="checkbox" <?php checked( $data['add_open_div'] == 1 ) ?> name="virtooal_settings[add_open_div]" id="virtooal-add_open_div" value="1">
                                Add "Try On" button next to "Add to Cart" button on product page
                            </label>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="virtooal-tryon_text">Try On button text</label>
                    </th>
                    <td>
                        <input type="text" name="virtooal_settings[tryon_text]" id="virtooal-tryon_text" class="regular-text" value="<?php echo $data['tryon_text'];?>">
                    </td>
                </tr>
            </tbody>
        </table>
        <?php submit_button(); ?>
    </form>

<?php elseif( $tab == 'product_feed' ): ?>
    <h2>Product Feed Settings</h2>
    <p>
        XML Feed URL: 
        <a href="<?php echo site_url('/?virtooal_xml_product_feed'); ?>" target="_blank" rel="noopener">
            <?php echo site_url('/?virtooal_xml_product_feed'); ?>
        </a>
    </p>
    <form action="<?php echo $formAction; ?>" method="post" id="virtooal_product_feed_form" >
        <input type="hidden" name="action" value="virtooal_product_feed_response">
        <input type="hidden" name="virtooal_product_feed_nonce" value="<?php echo wp_create_nonce( 'virtooal_product_feed_form_nonce' ) ?>"/>
        <table class="form-table" aria-label="Product Feed Settings">
            <tbody>
                <tr>
                    <th scope="row">
                        <label for="virtooal-status">Status</label>
                    </th>
                    <td>
                        <select name="virtooal_product_feed[status]" id="virtooal-status">
                            <option value="1" <?php selected((int)$data['status'] == 1); ?>>Enabled</option>
                            <option value="0" <?php selected((int)$data['status'] == 0); ?>>Disabled</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        Post Status
                    </th>
                    <td>
                        <fieldset>
                            <label for="virtooal-post_status_publish">
                                <input type="checkbox" <?php checked( in_array( 'publish', $data['post_statuses'] ) ); ?>
                                name="virtooal_product_feed[post_statuses][]"
                                value="publish" id="virtooal-post_status_publish">Published
                            </label>
                            <br>
                            <label for="virtooal-post_status_pending">
                                <input type="checkbox" <?php checked( in_array( 'pending', $data['post_statuses'] ) ); ?>
                                name="virtooal_product_feed[post_statuses][]" 
                                value="pending" id="virtooal-post_status_pending">
                                Pending
                            </label>
                            <br>
                            <label for="virtooal-post_status_draft">
                                <input type="checkbox" <?php checked( in_array( 'draft', $data['post_statuses'] ) ); ?>
                                name="virtooal_product_feed[post_statuses][]" 
                                value="draft" id="virtooal-post_status_draft">
                                Draft
                            </label>
                            <br>
                            <label for="virtooal-post_status_future">
                                <input type="checkbox" <?php checked( in_array( 'future', $data['post_statuses'] ) ); ?>
                                name="virtooal_product_feed[post_statuses][]"
                                value="future" id="virtooal-post_status_future">
                                Future
                            </label>
                            <br>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="virtooal-default_category">Default Category</label>
                    </th>
                    <td>
                        <select name="virtooal_product_feed[default_category]" id="virtooal-default_category">
                            <option value=""></option>
                            <?php foreach ($virtooal_categories as $group_name => $group): ?>
                            <optgroup label="<?php echo ucfirst($group_name); ?>">
                                <?php foreach ($group as $category_id => $category_name): ?>
                                <option value="<?php echo $category_id; ?>" <?php selected($data['default_category'] == $category_id) ?>>
                                    <?php echo $category_name; ?>
                                </option>
                                <?php endforeach; ?>
                            </optgroup>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="virtooal-default_gender">Default Gender</label>
                    </th>
                    <td>
                        <select name="virtooal_product_feed[default_gender]" id="virtooal-default_gender">
                            <option value=""></option>
                            <?php foreach ($virtooal_genders as $gender_id => $gender_name): ?>
                            <option value="<?php echo $gender_id; ?>" <?php selected($data['default_gender'] == $gender_id) ?>>
                                <?php echo $gender_name; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Category Pairing</th>
                    <td>
                        <table aria-label="Product Feed Settings - Category Pairing">
                            <tr>
                                <th scope="col" style="width: auto; padding: 20px 10px;">Store's Product Category</th>
                                <th scope="col" style="width: auto; padding: 20px 10px;">Virtooal's Product Category</th>
                                <th scope="col" style="width: auto; padding: 20px 10px;">Virtooal's Gender</th>
                                <th scope="col" style="width: auto; padding: 20px 10px;">Export</th>
                            </tr>
                        <?php foreach ($store_categories as $ancestors => $product_category): ?>
                            <tr>
                                <td>
                                    <label for="virtooal-categories_<?php echo $product_category->term_id; ?>"><?php echo $ancestors; ?></label>
                                </td>
                                <td>
                                    <select name="virtooal_product_feed[categories][<?php echo $product_category->term_id; ?>]" id="virtooal-categories_<?php echo $product_category->term_id; ?>">
                                        <option value=""></option>
                                        <?php foreach ( $virtooal_categories as $group_name => $group ): ?>
                                        <optgroup label="<?php echo ucfirst($group_name); ?>">
                                            <?php foreach ( $group as $category_id => $category_name ): ?>
                                            <option value="<?php echo $category_id; ?>" 
                                                <?php selected( isset( $data['categories'][$product_category->term_id] ) && $data['categories'][$product_category->term_id] == $category_id ) ?>>
                                                <?php echo $category_name; ?>
                                            </option>
                                            <?php endforeach; ?>
                                        </optgroup>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="virtooal_product_feed[genders][<?php echo $product_category->term_id; ?>]" id="virtooal-genders_<?php echo $product_category->term_id; ?>">
                                        <option value=""></option>
                                        <?php foreach ($virtooal_genders as $gender_id => $gender_name): ?>
                                        <option value="<?php echo $gender_id; ?>" <?php selected( isset( $data['genders'][$product_category->term_id] ) && $data['genders'][$product_category->term_id] == $gender_id ) ?>>
                                            <?php echo $gender_name; ?>
                                        </option>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="virtooal_product_feed[export][<?php echo $product_category->term_id; ?>]" id="virtooal-export_<?php echo $product_category->term_id; ?>">
                                        <option value="1" <?php selected( !isset( $data['export'][$product_category->term_id] ) || $data['export'][$product_category->term_id] == 1) ?>>Yes</option>
                                        <option value="0" <?php selected(isset( $data['export'][$product_category->term_id] ) && $data['export'][$product_category->term_id] == 0) ?>>No</option>
                                    </select>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php submit_button(); ?>
    </form>

<?php elseif( $tab == 'api' ):?>
    <h2>Virtooal API Connection</h2>
    <?php if($data['refresh_token']): ?>

    <p>You are connected to Virtooal API with API key: <strong><?php echo $data['public_api_key'] ?></strong></p>
    <form action="<?php echo $formAction; ?>" method="post" id="virtooal_api_logout_form" >
        <input type="hidden" name="action" value="virtooal_api_logout_response">
        <input type="hidden" name="virtooal_api_logout_nonce" value="<?php echo wp_create_nonce( 'virtooal_api_logout_form_nonce' ) ?>"/>
        <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Disconnect"></p>
    </form>

    <?php else: ?>
    <form action="<?php echo $formAction; ?>" method="post" id="virtooal_api_login_form" >
        <input type="hidden" name="action" value="virtooal_api_login_response">
        <input type="hidden" name="virtooal_api_login_nonce" value="<?php echo wp_create_nonce( 'virtooal_api_login_form_nonce' ) ?>"/>
        <table class="form-table" aria-label="API Connection Settings">
            <tbody>
                <tr>
                    <th scope="row">
                        <label for="virtooal-public_api_key">API Key<span class="woocommerce-help-tip"></span></label>
                    </th>
                    <td>
                        <input required name="virtooal[public_api_key]" id="virtooal-public_api_key" class="regular-text" type="text">
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <label for="virtooal-private_api_key">Private API Key<span class="woocommerce-help-tip"></span></label>
                    </th>
                    <td>
                        <input required name="virtooal[private_api_key]" id="virtooal-private_api_key" class="regular-text" type="password">
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Connect"></p>
    </form>
    <?php endif; ?>
<?php endif;  ?>

</div>