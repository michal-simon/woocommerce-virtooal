<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$status = 'n/a';
if( $in_virtooal_db ) {
	$status = $published ? 'Published' : 'Unpublished';
}
?>			
<p>
	<span class="dashicons dashicons-post-status"></span>
	Virtooal Status: <strong><?php echo $status; ?></strong>
</p>
<p>
	<a href="//setup.virtooal.com/en/auth/index?<?php echo $query_data; ?>" class="button" target="_blank">
		<?php echo $in_virtooal_db ? 'Edit in' : 'Add to'; ?> Virtual Mirror
	</a>
</p>