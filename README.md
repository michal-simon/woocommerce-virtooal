# Woocommerce Virtooal

[![Codeac.io](https://static.codeac.io/badges/1-289e15ba-902c-43c0-b0a3-580de5592565.svg "Codeac.io")](https://app.codeac.io/bitbucket/virtooal/woocommerce-virtooal)

The Virtual mirror allows the shoppers to experience all decorative cosmetics, sunglasses, contact lenses, jewelry,
clothing and apparel using their own photos or photos of models. It is suitable for all shops.
The editing process of the profile picture works as a process of auto-detection of the face and its features (lips, eyes, etc.),
no real time editing needed.

## Live Demo

Test our solution in a fully working [demo store](https://try.virtooal.com/en/demo-store).

## Requirements
- PHP 7.1.1+
